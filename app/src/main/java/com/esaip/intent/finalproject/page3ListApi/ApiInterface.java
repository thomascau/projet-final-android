package com.esaip.intent.finalproject.page3ListApi;

import retrofit2.Call;
import retrofit2.http.GET;


/**
 * Created by Alexandre on 23/01/2018.
 */

public interface ApiInterface {

    @GET("3/discover/movie?api_key=e99deed2024192766229e0934309275b&primary_release_date.gte=2014-09-15&primary_release_date.lte=2014-10-22")
    Call<MovieApi> getMovie();
}
