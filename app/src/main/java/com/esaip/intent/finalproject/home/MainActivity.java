package com.esaip.intent.finalproject.home;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.esaip.intent.finalproject.page1Map.Page1;
import com.esaip.intent.finalproject.page2ListRecycler.Page2;
import com.esaip.intent.finalproject.page3ListApi.Page3;
import com.esaip.intent.finalproject.R;

public class MainActivity extends AppCompatActivity {
    Button button1, button2, button3, button4, buttonClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        button1 = findViewById(R.id.button1);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Page1.class);
                startActivity(intent);
            }
        });

        button2 = findViewById(R.id.button2);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Page2.class);
                startActivity(intent);
            }
        });


        buttonClose = findViewById(R.id.btn_close);
        buttonClose.setVisibility(View.INVISIBLE);

        button3 = findViewById(R.id.btn_frag1);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonClose.setVisibility(View.VISIBLE);
                FragmentManager FM = getFragmentManager();
                FragmentTransaction FT = FM.beginTransaction();
                FragmentOne F1 = new FragmentOne();
                FT.add(R.id.fragment1, F1);
                FT.addToBackStack("f1");
                FT.commit();
            }
        });

        button4 = findViewById(R.id.button4);
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("test", "onclick");
                Intent intent = new Intent(MainActivity.this, Page3.class);
                startActivity(intent);
            }
        });


        FragmentManager FM = getFragmentManager();
        FragmentTransaction FT = FM.beginTransaction();
        FragmentTwo F2 = new FragmentTwo();
        FT.add(R.id.fragment1, F2);
        FT.addToBackStack("f2");
        FT.commit();

        buttonClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonClose.setVisibility(View.INVISIBLE);
                FragmentManager FM = getFragmentManager();
                FragmentTransaction FT = FM.beginTransaction();
                FragmentTwo F2 = new FragmentTwo();
                FT.add(R.id.fragment1, F2);
                FT.addToBackStack("f2");
                FT.commit();

            }
        });


    }
}
