package com.esaip.intent.finalproject.page3ListApi;

/**
 * Created by Alexandre on 25/01/2018.
 */

public class Results
{
    private Number vote_average;

    private String backdrop_path;

    private boolean adult;

    private int id;

    private String title;

    private String overview;

    private String original_language;

    public Number getVote_average() {
        return vote_average;
    }

    public String getBackdrop_path() {
        return backdrop_path;
    }

    public boolean isAdult() {
        return adult;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getOverview() {
        return overview;
    }

    public String getOriginal_language() {
        return original_language;
    }
}
