package com.esaip.intent.finalproject.page3ListApi;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.esaip.intent.finalproject.R;
import com.esaip.intent.finalproject.home.MainActivity;
import com.esaip.intent.finalproject.page1Map.LifecycleLoggingActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Alexandre on 23/01/2018.
 */

public class Page3 extends LifecycleLoggingActivity {

    private Button listButton, retourButton;
    private RecyclerView recyclerView;
    private MoviesAdapter mAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page3);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        mAdapter = new MoviesAdapter();
        recyclerView.setAdapter(mAdapter);

        listButton = findViewById(R.id.list);
        listButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moovieList();
            }
        });

        retourButton = findViewById(R.id.retour);
        retourButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Page3.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }

    private void moovieList(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.themoviedb.org/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                ;


        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<MovieApi> movies = service.getMovie();
        movies.enqueue(new Callback<MovieApi>() {
            @Override
            public void onResponse(Call<MovieApi> call, Response<MovieApi> response) {
                MovieApi movieApi = response.body();

                List<Results> movies = new ArrayList<>();
                for (Results item : movieApi.getResults()){
                    movies.add(item);
                }
                mAdapter.setData(movies);
            }

            @Override
            public void onFailure(Call<MovieApi> call, Throwable t) {
            }
        });



    }
}
