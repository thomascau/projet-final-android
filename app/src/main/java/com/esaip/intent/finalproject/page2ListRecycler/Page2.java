package com.esaip.intent.finalproject.page2ListRecycler;

/**
 * Created by Thomas on 23/01/2018.
 */

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.esaip.intent.finalproject.R;

import java.util.ArrayList;
import java.util.List;

public class Page2 extends AppCompatActivity {
    private List<Movie> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private MoviesAdapterList mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page2);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);



        mAdapter = new MoviesAdapterList(movieList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(mAdapter);


        prepareMovieData();
    }

    private void prepareMovieData() {
        Movie movie = new Movie("Jumanji", " Fantastique, Action", "20 décembre 2017");
        movieList.add(movie);

        movie = new Movie("Ferdinand", "Animation, Comédie, Aventure", "20 décembre 2017");
        movieList.add(movie);

        movie = new Movie("La Promesse de l'aube", "Comédie dramatique, Drame", "27 décembre 2017");
        movieList.add(movie);

        movie = new Movie("Normandie Nue", "Drame, Comédie", "10 janvier 2018");
        movieList.add(movie);

        movie = new Movie("Star Wars", "Science Fiction", "13 décembre 2017");
        movieList.add(movie);

        movie = new Movie("Coco", " Animation, Fantastique", "29 novembre 2017");
        movieList.add(movie);

        movie = new Movie("Downsizing", "Comédie", "10 janvier 2018");
        movieList.add(movie);

        movie = new Movie("Momo", "Comédie", "27 décembre 2017");
        movieList.add(movie);

        movie = new Movie("L'Orient-Express", "Thriller, Policier", "13 décembre 2017");
        movieList.add(movie);

        movie = new Movie("Brillantissime", "Comédie", "17 janvier 2018");
        movieList.add(movie);

        movie = new Movie("Les heures sombres", "Drame", "3 janvier 2018");
        movieList.add(movie);

        movie = new Movie("Le Grand jeu", "Drame, Biopic", "3 janvier 2018");
        movieList.add(movie);

        movie = new Movie("Paddington 2", "Animation, Comédie", "6 décembre 2017");
        movieList.add(movie);

        movie = new Movie("Wonder", "Drame, Famille", "20 décembre 2017");
        movieList.add(movie);

        movie = new Movie("Ami-ami", "Comédie", "17 janvier 2018");
        movieList.add(movie);

        movie = new Movie("Garde alternée", "Comédie", "20 décembre 2017");
        movieList.add(movie);

        mAdapter.notifyDataSetChanged();
    }
}
