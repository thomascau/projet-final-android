package com.esaip.intent.finalproject.page1Map;

/**
 * Created by Thomas on 23/01/2018.
 */

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.esaip.intent.finalproject.R;
import com.esaip.intent.finalproject.home.MainActivity;

public class Page1 extends LifecycleLoggingActivity {
    private String TAG = getClass().getSimpleName();
    private EditText mEditText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page1);

        Button mAddButton = findViewById(R.id.btn_add);
        mEditText = findViewById(R.id.location);
        mAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startMap();
            }
        });


        Button buttonMenu = findViewById(R.id.buttonMenu);
        buttonMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Page1.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }


    private void startMap(){

        try {
            String address = mEditText.getText().toString();
            final Intent geoIntent = makeMapsIntent(address);

            if (geoIntent.resolveActivity(getPackageManager()) != null)
                startActivity(geoIntent);
            else
                startActivity(makeBrowserIntent(address));
        } catch (Exception e){
            e.printStackTrace();
        }

    }


    private Intent makeMapsIntent(String address){

        return new Intent(Intent.ACTION_VIEW,
                Uri.parse("geo:0,0?q="
                        + Uri.encode(address)+" cinéma"));

    }

    private Intent makeBrowserIntent(String address){

        Intent intent = new Intent (Intent.ACTION_VIEW,
                Uri.parse("https://maps.google.com/?q="
                        + Uri.encode(address)));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        return intent;
    }
}
