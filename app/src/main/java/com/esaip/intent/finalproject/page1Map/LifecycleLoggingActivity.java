package com.esaip.intent.finalproject.page1Map;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

/**
 * Created by Thomas on 22/01/2018.
 */

public abstract class LifecycleLoggingActivity extends Activity {

    private final String TAG = getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        if(savedInstanceState !=null){
            Log.d(TAG, "onCreate() : activity re-created");
        }
    }

    @Override
    protected void onStart(){
        super.onStart();
        Log.d(TAG, "onStart() - the activity is about to become visible");

    }

    @Override
    protected void onResume(){
        super.onResume();
        Log.d(TAG,"onResume() - the activity has become visible (it is now " +"\"resumed\")");

    }

    @Override
    protected void onPause(){
        super.onPause();
        Log.d(TAG,"onPause() - another activity is taking focus (this activity " +"\"paused\")");

    }

    @Override
    protected void onStop(){
        super.onStop();
        Log.d(TAG,"onStop() - the activity is no longer visible (it is now " +"\"stopped\")");

    }

    @Override
    protected void onRestart(){
        super.onRestart();
        Log.d(TAG,"onRestart() - the activity is about to be restarted");

    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        Log.d(TAG,"onDestroy() - the activity is about to be destroyed");

    }



}
