package com.esaip.intent.finalproject;

import com.esaip.intent.finalproject.page2ListRecycler.Movie;

import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class UnitTestApp {
    @Test
    public void movieTitle_isCorrect() throws Exception {
        Movie movie = new Movie("succes get title", " hello", "10 janvier 2018");
        String title = movie.getTitle();
        System.out.print("Test du getTitle() retourne : " + title +"\n");

        movie.setTitle("succes set title");
        title = movie.getTitle();
        System.out.print("Test du setTitle() retourne : " + title+"\n\n");

    }

    @Test
    public void movieYear_isCorrect() throws Exception {
        Movie movie = new Movie("hello", " world", "succes get year");
        String year = movie.getYear();
        System.out.print("Test du getYear() retourne : " + year +"\n");

        movie.setYear("succes set year");
        year = movie.getYear();
        System.out.print("Test du setYear() retourne : " + year+"\n\n");

    }

    @Test
    public void movieGenre_isCorrect() throws Exception {
        Movie movie = new Movie("hello", " succes get genre", "world");
        String genre = movie.getGenre();
        System.out.print("Test du getGenre() retourne : " + genre +"\n");

        movie.setGenre("succes set genre");
        genre = movie.getGenre();
        System.out.print("Test du setGenre() retourne : " + genre+"\n\n");

    }
}